//
//  GameScene.swift
//  Test34-S19
//
//  Created by MacStudent on 2019-06-19.
//  Copyright © 2019 rabbit. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    
    var nextLevelButton:SKLabelNode!
    
    
    
    var mouseStartingPosition:CGPoint = CGPoint(x:0, y:0)
    var lastSpriteTouched:SKNode!
    var firstSpriteTouched:SKNode!
    
    
    
    
    
    override func didMove(to view: SKView) {
        print("This is level 1")
        self.nextLevelButton = self.childNode(withName: "nextLevelButton") as? SKLabelNode
        
        
        
        
    }
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
    }
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        if (touch == nil) {
            return
        }
        
        let location = touch!.location(in:self)
        let node = self.atPoint(location)
        
        
        
        
        // MARK: Switch Levels
        if (node.name == "nextLevelButton") {
            let scene = SKScene(fileNamed:"Level2")
            if (scene == nil) {
                print("Error loading level")
                return
            }
            else {
                scene!.scaleMode = .aspectFill
                view?.presentScene(scene!)
            }
        }
        
        // Tracking the user touch from start to end
        
        let touchBegin = touches.first!
        let mousePositionFirst = touchBegin.location(in:self)
        
        self.mouseStartingPosition = mousePositionFirst;
        self.firstSpriteTouched = self.atPoint(mousePositionFirst)
        
        removeHair()
        
       
        
        
        
    }
   
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touchEnd = touches.first!
        let mousePositionLast = touchEnd.location(in:self)
        
        self.lastSpriteTouched = self.atPoint(mousePositionLast)
        //print("Starting position: \(mouseStartingPosition.x), \(mouseStartingPosition.y)")
       // print("Ending position: \(mousePositionLast.x), \(mousePositionLast.y)")
        removeHair()
    }
    
    func removeHair() {
        
    // Removing the sprites/hair from scene
        if (firstSpriteTouched?.name == "hair1" && lastSpriteTouched?.name == "hair4") {
            firstSpriteTouched.removeFromParent()
            lastSpriteTouched.removeFromParent()
            
            for child in self.children{
                
                if (child.name == "hair2" || child.name == "hair3") {
                    child.removeFromParent()
                }
            }
           
        }
    }
 

}
