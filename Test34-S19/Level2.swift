//
//  GameScene.swift
//  Test34-S19
//
//  Created by MacStudent on 2019-06-19.
//  Copyright © 2019 rabbit. All rights reserved.
//

import SpriteKit
import GameplayKit

class Level2: SKScene {
    // for initial commit
    
    var nextLevelButton:SKLabelNode!
    
    
    override func didMove(to view: SKView) {
        print("Loaded level 2")
        self.nextLevelButton = self.childNode(withName: "nextLevelButton") as? SKLabelNode
        
        
        
        
        // code to spawn 15 lemmings with a delay
        for _ in 1...15 {
        let generateLemmingSequence = SKAction.sequence(
            [
                SKAction.run {
                    [weak self] in self?.makeLemmings()
                },
                SKAction.wait(forDuration: 0.5)
            ]
        )
        self.run(SKAction.repeatForever(generateLemmingSequence))
        
        }
        
        
        
    }
    let lemming = SKSpriteNode(imageNamed: "player1")
   
    func makeLemmings()  {
        // Adding lemmings
        
        
        
        // spawing lemmings at entrance position
        lemming.position = CGPoint(x:410.62, y:894.04)
        
        
        addChild(lemming)
        
    }
    
    
    func makePlatform(xPosition:CGFloat, yPosition:CGFloat) {
        
        // 1. create a platform
        let platform = SKSpriteNode(imageNamed: "platform")
        platform.physicsBody?.affectedByGravity = false     // turning off gravity
        platform.physicsBody?.isDynamic = false             // turning off collisions


        
    }
    
    
    var lemmingDirection: String = "right"
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
        // 2. Move him 10px each time
        if (lemmingDirection == "right") {
            lemming.position = CGPoint(x:lemming.position.x + 10,
                                      y:400)
        }
        else if (lemmingDirection == "left") {
            lemming.position = CGPoint(x:lemming.position.x - 10,
                                      y:400)
        }
        
        // 3. Detect when he hits the wall
        
        // R1:  Hit right wall, go left
            if (lemming.position.x >= 800) {
            print("HIT THE WALL")
            lemmingDirection = "left"
        }
        
        // R2:  Hit left wall, go right
        if (lemming.position.x <= 0) {
            lemmingDirection = "right"
        }
        
       // Creating hitbox for blocks
            var block = SKSpriteNode()
            block = self.childNode(withName:"platform") as! SKSpriteNode
            let blockBodySize = CGSize(width: 70, height: 50)
        
        
        // Assigning cateory masks
        struct PhysicsCategory {
            static let None:  UInt32 = 0
            static let platform:   UInt32 = 0b1      // 0x00000001 = 1
            static let block: UInt32 = 0b10     // 0x00000010 = 2
            static let exit:   UInt32 = 0b100    // 0x00000100 = 4
            static let entrance: UInt32 = 0b1000   // 0x00001000 = 8
        }
        
        self.lemming.physicsBody!.contactTestBitMask =
            PhysicsCategory.platform | PhysicsCategory.block


        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        if (touch == nil) {
            return
        }
        
        let location = touch!.location(in:self)
        let node = self.atPoint(location)
        
        
        
        
        // MARK: Switch Levels
        
        if (node.name == "nextLevelButton") {
            let scene = SKScene(fileNamed:"Level3")
            if (scene == nil) {
                print("Error loading level")
                return
            }
            else {
                scene!.scaleMode = .aspectFill
                view?.presentScene(scene!)
            }
        }
        
        
        let touch2 = touches.first!
        let mousePosition = touch2.location(in:self)
        
        // 2. make a platform in the same position as mouse click
        self.makePlatform(xPosition: mousePosition.x, yPosition: mousePosition.y)
        
    }
}
